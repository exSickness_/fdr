#!/usr/bin/env python3

import socketserver
import threading
import time
import os

"""
REFERENCES
Roman numeral conversions - http://code.activestate.com/recipes/81611-roman-numerals/
socketserver - https://docs.python.org/3.4/library/socketserver.html
Listening on multiple ports - http://stackoverflow.com/questions/3655053/python-listen-on-two-ports
Threading - https://docs.python.org/3/library/threading.html
"""

class ThreadedUDPServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
	pass

class ThreadedUDPRequestHandler(socketserver.BaseRequestHandler):
	def handle(self):
		""" Method for handling UDP requests coming in """
		# Get data about request
		data = self.request[0].strip()
		socket = self.request[1]

		print(self.client_address, "Sent:", data)

		# Decode from bytes and seperate type of request
		data = data.decode('utf-8')
		pType = data[0].upper()

		# Strip out type of request from actual data
		data = data[1:]
		result = -1

		if(pType == 'F'):
			# Type is a fibonacci packet, handle accordingly
			if data.isnumeric():
				data = int(data)
				if data <= 300 and data >= 0:
					result = fNumber(data)
					result = str(hex(result))
			else:
				result = -1

		elif(pType == 'D'):
			# Type is a decimal packet, handle accordingly
			if data.isnumeric() and len(data) <= 32:
				data = int(data)
				result = dNumber(data)
			else:
				result = -1

		elif(pType == 'R'):
			# Type is a roman packet, handle accordingly
			result = rNumber(data)
			if result != -1:
				result = str(hex(result))

		else:
			pass

		# Result will be -1 if at any time, an invalid piece of data was encountered
		if(result == -1):
			# Send "Invalid request." to sender
			invalidStr = "Invalid request."
			print(invalidStr)
			invalidStr += '\0'
			toSend = bytes(invalidStr, 'utf8')
			socket.sendto(toSend, self.client_address)
			return()

		# No invalid data parsed, send back result
		print("Sending back:", result)
		result += '\0'
		toSend = bytes(result, 'utf8')
		socket.sendto(toSend, self.client_address)

def fNumber(n):
	print("Fibonacci packet received")

	# Iteratively calculate fibonacci number
	n1 = 1
	n2 = 0
	total = 0
	for x in range(n):
		total = n1 + n2
		n1 = n2
		n2 = total
	return(total)

def dNumber(n):
	print("Decimal packet received")

	# Double check type and return number, hexed.
	if type(n) != type(1):
		return(-1)
	return(str(hex(n)))


def rNumber(rNumStr):
	print("Roman packet received")

	rNumStr = rNumStr.upper()
	nums = ['M', 'D', 'C', 'L', 'X', 'V', 'I']
	ints = [1000, 500, 100, 50,  10,  5,   1]
	places = []

	# Check each letter for validity
	for c in rNumStr:
		if not c in nums:
			print("Not valid roman numeral.")
			return(-1)

	# Loop to calculate total value
	for i in range(len(rNumStr)):
		c = rNumStr[i]
		value = ints[nums.index(c)]
		# If the next place holds a larger number, this value is negative.
		try:
			nextvalue = ints[nums.index(rNumStr[i +1])]
			if nextvalue > value:
				value *= -1
		except IndexError:
			# there is no next place.
			pass
		places.append(value)

	# Add up value in each place and test it.
	retVal = 0
	for n in places:
		retVal += n
	if validateRoman(retVal) == rNumStr:
		return(retVal)
	else:
		print("Not valid roman numeral.")
		return(-1)

def validateRoman(n):
	if type(n) != type(1):
		print("Invalid type.")
	if not 0 < n <= 4000:
		print("Invalid number range.")
	ints = (1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1)
	nums = ('M',  'CM', 'D', 'CD','C', 'XC','L','XL','X','IX','V','IV','I')
	result = ""
	for i in range(len(ints)):
		count = int(n / ints[i])
		result += nums[i] * count
		n -= ints[i] * count
	return result


if __name__ == "__main__":
	# Set up host and ports to connect to
	uid = os.getuid()
	HOST = "localhost"
	PORT_A = uid
	PORT_B = uid+1000
	PORT_C = udi+2000

	try:
		# Create a server for each port
		server_A = ThreadedUDPServer((HOST, PORT_A), ThreadedUDPRequestHandler)
		server_B = ThreadedUDPServer((HOST, PORT_B), ThreadedUDPRequestHandler)
		server_C = ThreadedUDPServer((HOST, PORT_C), ThreadedUDPRequestHandler)

		# Create a thread for each port
		server_A_thread = threading.Thread(target=server_A.serve_forever)
		server_B_thread = threading.Thread(target=server_B.serve_forever)
		server_C_thread = threading.Thread(target=server_C.serve_forever)

		# Make sure they shut down correctly
		server_A_thread.setDaemon(True)
		server_B_thread.setDaemon(True)
		server_C_thread.setDaemon(True)

		# Start the threads
		server_A_thread.start()
		server_B_thread.start()
		server_C_thread.start()

		# Run each thread and wait until ctrl+c is entered.
		while 1:
			time.sleep(1)

	except KeyboardInterrupt:
		print("Exiting.")

	finally:
		# Shut down servers
		print("Shutting down servers.")
		server_A.shutdown()
		server_B.shutdown()
		server_C.shutdown()
		print("Shut down all servers.")

		# Join threads
		print("Joining server threads.")
		server_A_thread.join()
		server_B_thread.join()
		server_C_thread.join()
		print("Successfully joined server threads.")
